package main

import (
	"fmt"
	"strconv"
)

type Spielfeld struct {
	Feld   [][]string
	Größe  int
	Breite int
	Höhe   int
}

// Konstruktor für ein Spielfeld
func MakeSpielfeld(anzahlSpalten, anzahlZeilen int) Spielfeld {

	feld := make([][]string, anzahlZeilen)
	for i := 0; i < anzahlZeilen; i++ {
		feld[i] = make([]string, anzahlSpalten)
		for x := range feld[i] {
			feld[i][x] = " " // -> jede Liste wird mit Leerzeichen befüllt (wegen Ausgabe)
		}
	}
	spielfeld := Spielfeld{feld, anzahlSpalten * anzahlZeilen, anzahlSpalten, anzahlZeilen}
	return spielfeld

}

// Methode zur Überprüfung von Eingaben auf dem Spielfeld
func (sf Spielfeld) checkEingabe(eingabe string) bool {

	convEingabe, error := strconv.Atoi(eingabe)
	if !(error == nil && convEingabe >= 1 && convEingabe <= sf.Größe) {
		return false
	}

	xpos, ypos := sf.getPos(eingabe)
	for i, v := range sf.Feld {
		if i == ypos-1 && v[xpos] != " " {
			return false
		}
	}
	return true

}

// Methode zur Eingabe eines Spielerzeichens an eine beliebige Stelle einer [][]string slice
func (sf *Spielfeld) setEingabe(posEingabeS string, zeichen string) {

	xpos, ypos := sf.getPos(posEingabeS)

	// Zeichen an Listenposition schreiben
	for i, v := range sf.Feld {
		if i == ypos-1 {
			v[xpos] = zeichen
		}
	}
}

// Funktion zur Bestimmung der Spielfeldkoordinaten einer Eingbae
func (sf Spielfeld) getPos(posEingabeS string) (int, int) {

	posEingabe, _ := strconv.Atoi(posEingabeS) // -> Typkonvertierung string to int

	var ypos, xpos int
	if posEingabe%sf.Breite != 0 {
		ypos = posEingabe/sf.Breite + 1
	} else {
		ypos = posEingabe / sf.Breite
	}

	if posEingabe%sf.Breite == 0 {
		xpos = posEingabe%sf.Breite + sf.Breite - 1
	} else {
		xpos = posEingabe%sf.Breite - 1
	}
	return xpos, ypos
}

// Methode zur Ausgabe des Spielfeldes (Könnte man noch anpassen auf größe der Listenelemente)
func (sf Spielfeld) Ausgabe() {
	b := sf.Feld
	fmt.Println()
	for i := range b[0] {
		if i == 0 {
			fmt.Printf("  | %v", i+1)
		} else {
			fmt.Printf(" | %v", i+1)
		}
	}
	fmt.Println(" |")
	for i := range b[0] {
		if i > 0 {
			fmt.Printf("+---")
		} else {
			fmt.Print("--+---")
		}
	}
	fmt.Println("+")
	for i, row := range b {
		fmt.Printf("%v | ", i+1)
		for _, c := range row {
			fmt.Printf(string(c) + " | ")
		}
		fmt.Println()
		if i < len(b)-1 {

			for i := range b[0] {

				if i == 0 {
					fmt.Printf("--+---")
				} else {
					fmt.Print("+---")
				}
			}
			fmt.Println("+")
		}

	}
	fmt.Println()
}
