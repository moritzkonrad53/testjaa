package main

import (
	"fmt"
)

func TikTakToeMain() {

	fmt.Println()

	TikTakToe := MakeNeuesSpiel(3, 3, 2) // Feldgröße und Anzahl Spieler hardcodiert!
	TikTakToe.addPlayer()

	Spielen(&TikTakToe)
	if !TikTakToe.IsDraw() {
		fmt.Printf("%v gewinnt das Spiel!", TikTakToe.aktPlayer.Name)
	} else {
		fmt.Printf("Unentschieden!")
	}
}

func Spielen(spiel *Spiel) {

	for !spiel.GameOver() {

		spiel.DecidePlayersTurn()
		spiel.Spielzug()
		spiel.Spielfeld.Ausgabe()

	}

}
