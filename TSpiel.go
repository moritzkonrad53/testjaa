package main

import "fmt"

type Spiel struct {
	Spielfeld Spielfeld
	Spieler   []Spieler
	aktRunde  int
	aktPlayer Spieler //Spieler der gerade am Zug ist
}

// Konstruktor für ein neues Spiel (erstmal nur TikTakToe)
func MakeNeuesSpiel(anzahlZeilen, anzahlSpalten, AnzahlSpieler int) Spiel {
	return Spiel{Spielfeld: MakeSpielfeld(anzahlSpalten, anzahlZeilen),
		Spieler:  make([]Spieler, AnzahlSpieler),
		aktRunde: 0}
}

// Methode zur eingabe der Spielernamen && Spielerzeichen werden festgelegt -> bisher nur X,O -> max zwei Spieler
func (s *Spiel) addPlayer() {
	var eingabe string
	Spielerzeichen := []string{"X", "O"}
	for i := range s.Spieler {
		fmt.Printf("Name von Spieler %v eingeben ", i+1)
		fmt.Scanln(&eingabe)
		s.Spieler[i] = Spieler{eingabe, Spielerzeichen[i], false}
		//s.Spieler[i].Name = eingabe
		//s.Spieler[i].Zeichen = Spielerzeichen[i]
	}
}

// Methode für einen Spielzug
func (s *Spiel) Spielzug() {

	s.aktRunde += 1

	if s.aktRunde == 1 {
		s.Spielfeld.Ausgabe()
	}

	// Spielfeldeingabe machen,checken und setzen
	var eingabe string
	fmt.Printf("%v, wo möchtest du dein Zeichen setzen? ", s.aktPlayer.Name)
	fmt.Scanln(&eingabe)
	for !s.Spielfeld.checkEingabe(eingabe) {
		fmt.Print("Ungültige Eingabe! Andere Stelle wählen. ")
		fmt.Scanln(&eingabe)
	}

	s.Spielfeld.setEingabe(eingabe, s.aktPlayer.Zeichen)

}

// Methode um den Spieler der am Zug ist zu bestimmen
func (s *Spiel) DecidePlayersTurn() {

	// amZug Attribute verändern
	for i, v := range s.Spieler {

		if v.amZug {
			s.Spieler[(i+1)%len(s.Spieler)].amZug = true
			s.Spieler[i].amZug = false
			break
		}
	}

	// s.aktPlayer Attribut zuweisen zu Spieler, dessen amZug Attribut true ist
	for _, v := range s.Spieler {
		if v.amZug {
			s.aktPlayer = v
			return
		}
	}

	// Veränderung des Attributes s.aktPlayer -> Bestimmung Startspieler
	s.Spieler[0].amZug = true
	s.aktPlayer = s.Spieler[0]
}

// Funktion prüft, ob einer der Endbedingungen zutrifft (Unentschieden oder Spieler gewinnt)
func (s Spiel) GameOver() bool {

	return matrixContains(s.Spielfeld.Feld, 3, s.aktPlayer.Zeichen) || s.IsDraw()

}

// Funktion prüft ob ein Untentschieden vorliegt
func (s Spiel) IsDraw() bool {

	for _, v := range s.Spielfeld.Feld {
		if Contains(v, " ") {
			return false
		}

	}

	return !matrixContains(s.Spielfeld.Feld, 3, s.aktPlayer.Zeichen)

}
