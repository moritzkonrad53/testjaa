package main

func Contains(list []string, x string) bool {
	for _, v := range list {
		if v == x {
			return true
		}
	}
	return false
}

// Funktion prüft ob eine Liste ausschließlich x enthält
func ContainsOnly(list []string, x string) bool {

	for _, v := range list {
		if v != x {
			return false
		}
	}
	return true

}

// Funktion prüft ob eine der Matrixreihen ausschließlich x enthält
func AnyListContainsOnly(sf [][]string, x string) bool {
	for _, v := range sf {
		if ContainsOnly(v, x) {
			return true
		}
	}
	return false

}

//Funktion liefert eine Matrix in der Zeilen und Spalten getauscht wurden (jede Spalte wird zur Zeile)
func getColumns(sf [][]string) [][]string {

	result := make([][]string, len(sf[0]))

	for i := 0; i < len(sf[0]); i++ {
		for _, row := range sf {
			result[i] = append(result[i], row[i])
		}
	}
	return result
}

// Funktion liefert eine Matrix mit den Hauptdiagonalen einer Quadratischen Matrix
func getMainDiags(f [][]string) [][]string {
	result := make([][]string, 2)
	for i := 0; i < 2; i++ {
		if i == 0 {
			for p, row := range f {
				result[i] = append(result[i], row[p])
			}
		} else {
			c := len(f[0])
			for _, row := range f {
				result[i] = append(result[i], row[c-1])
				c -= 1
			}
		}
	}
	return result
}

// Funktion liefert eine factor*factor große Matrix aus einer beliebig großen Matrix; x&y sind die Koordinaten für
// die (Start-)Position -> obere linke Ecke der neuen Matrix
func getMatrix(sf [][]string, factor int, x int, y int) [][]string {
	result := make([][]string, factor)
	for i := 0; i < factor; i++ {

		// Überprüfung ob Grenzen der Ausgansmatrix verletzt würden
		if x+factor <= len(sf[0]) && y+factor <= len(getColumns(sf)[0]) {
			result[i] = sf[y+i][x : x+factor]
		}
	}
	return result
}

// Funktion prüft ob eine Reihe, Spalte oder Diagonale der Länge length einer beliebig großen Matrix
// ausschließlich x enthält (sf wegen Spielfeld)
func matrixContains(sf [][]string, length int, x string) bool {
	var diag, column, row bool
	for c := 0; c <= len(getColumns(sf)[0])-length; c++ {
		for r := 0; r <= len(sf[0])-length; r++ {
			diag = AnyListContainsOnly(getMainDiags(getMatrix(sf, length, r, c)), x)
			column = AnyListContainsOnly(getColumns(getMatrix(sf, length, r, c)), x)
			row = AnyListContainsOnly(getMatrix(sf, length, r, c), x)
			if diag || column || row {
				return true
			}
		}

	}
	return false

}
